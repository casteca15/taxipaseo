$(document).on('ready', function(){
    var url = "http://pruebaplade.esy.es/";

    $(document).on('keyup', '.sololetra', function(event){
        var t = $(this).val().trim();
        if(isNaN(t[t.length-1]) == false){
            alert("No puede introducir numeros");
            $(this).val(t.substr(0, t.length-1));
        }
    });

    $(document).on('change', '.sololetra', function(event){
        var t = $(this).val().trim();
        var caja = $(this);
        for(i = 0; i < t.length; i++){
            if(isNaN(t[i]) == false){
                alert("No puede introducir numeros");
                caja.focus();
            }
        }
    });

    $("#formulario_registro").on('submit', function(){
        var pas = $("#formulario_registro input[name='pas']").val().trim();
        var cpa = $("#formulario_registro input[name='cpa']").val().trim();

        if($("#formulario_registro input[name='usu']").val().trim().length < 2){
            alert("El usuario debe tener minimo 2 caracteres");
            return false;
        }
        if($("#formulario_registro input[name='nom']").val().trim().length < 2){
            alert("El nombre debe tener minimo 2 caracteres");
            return false;
        }

        if($("#formulario_registro input[name='ape']").val().trim().length < 2){
            alert("El apellido debe tener minimo 2 caracteres");
            return false;
        }

        if(pas.length < 6 || cpa.length < 6){
            alert("La contraseña debe tener minimo 6 caracteres");
            return false;
        }
        if(pas != cpa){
            alert("Error: Las contraseñas no son iguales.");
            return false;
        }

        $.ajax({
             type: "POST",
             url: url+"Usuario.php",
             data: $("#formulario_registro").serialize(),
             crossDomain: true,
             cache: false,
             beforeSend: function(){ },
             success: function(data){
                data = data.trim();
                if(data == "ok"){
                    $("#formulario_registro").trigger('reset');
                    alert("Cuenta creada exitosamente");
                    $.mobile.changePage("#vt_login");
                }else if(data == "no"){
                    alert("Error: El usuario seleccionado ya esta en uso");
                }
             }
        });
        return false; 
    });

    $("#formulario_login").on('submit', function(){
        var usu = $("#formulario_login input[name='usu']").val().trim(); 
        var pas = $("#formulario_login input[name='pas']").val().trim(); 
        $.ajax({
             type: "POST",
             url: url+"Usuario.php",
             data: $("#formulario_login").serialize(),
             async: true,
             dataType: "json",
             timeout: 15000,
             contentType: "application/x-www-form-urlencoded",
             beforeSend: function(){ },
             success: function(data){
                data = data.trim();
                if(data == "okt"){
                    window.localStorage.setItem("sesion", true);
                    window.localStorage.setItem("usu", usu);
                    window.localStorage.setItem("pas", pas);
                    window.localStorage.setItem("tipo", "T");
                    obtenerMiDireccion();
                    alert("Bienvenido");
                    $("#formulario_login").trigger('reset');
                    $.mobile.changePage("#vt_principal_taxi");
                }else if(data == "oku"){
                    window.localStorage.setItem("sesion", true);
                    window.localStorage.setItem("usu", usu);
                    window.localStorage.setItem("pas", pas);
                    window.localStorage.setItem("tipo", "U");
                    obtenerMiDireccion();
                    alert("Bienvenido");
                    $("#formulario_login").trigger('reset');
                    $.mobile.changePage("#vt_principal_usuario");
                }else{
                    alert("Datos de acceso invalidos");
                }
             }, error: function(xhr, ajaxOptions, thrownError){
                  alert(xhr.status +  " " + thrownError);
             }
        });
        return false;
    });

    $("#formulario_taxi").on('submit', function(){
        if(confirm("Esta seguro de enviar esta solicitud?")){
            var usu = window.localStorage.getItem('usu');
            var da = $("#formulario_taxi").serializeArray();
            if($("#prioridad").val() == ""){
                alert("Seleccione prioridad");
                return false;
            }

            if($("#formulario_taxi input[name='dir']").val().trim().length < 4){
                alert("La dirección debe tener minimo 4 caracteres");
                return false;
            }

            da.push({name: 'usu', value: usu}); 
            $.post(url+'Taxi.php', da, function(data){
                console.log(data);
                if(data == "ok"){
                    alert("Su solicitud de taxi se envio correctamente, espere respuesta");
                    $("#formulario_taxi").trigger('reset');
                    $.mobile.changePage("#vt_principal_usuario");
                }else if(data == "pendiente"){
                    alert("Error: no puede enviar solicitudes mientras tenga una carrera en pendiente o en progreso");
                }else{
                    alert("Error: su solicitud no se pudo enviar, intente mas tarde.");
                }
            });

        }
        return false;
    });

    $(document).on('pagebeforeshow', '#vt_principal_taxi', function(){
        var usu = window.localStorage.getItem("usu");
        $("#historial_taxista").html(''); 
        $.post(url+"Taxi.php", {carreras:1, usu: usu}, function(data){
            $("#historial_taxista").append(data);
            $("#historial_taxista").trigger('create');
            realtime();
        });
    });

    $(document).on('pagebeforeshow', '#vt_principal_usuario', function(){
        var usu = window.localStorage.getItem("usu");
        $("#historial_enviadas").html(''); 
        $.post(url+"Taxi.php", {historial:1, usu: usu}, function(data){
            $("#historial_enviadas").append(data);
            $("#historial_enviadas").trigger('create');
            realtime();
        });
    });

    $(document).on('pagebeforeshow', '#vt_perfil', function(){
        var usu = window.localStorage.getItem("usu");
        var pas = window.localStorage.getItem("pas");
        $("#contenedor_perfil").html('');
        $.post(url+"Usuario.php", {perfil:1, usu: usu, pas: pas}, function(data){
            $("#contenedor_perfil").append(data);
            $("#contenedor_perfil").trigger('create');
        });
    });

    $(document).on('pagebeforeshow', '#vt_perfil_taxi', function(){
        var usu = window.localStorage.getItem("usu");
        var pas = window.localStorage.getItem("pas");
        $("#contenedor_perfil_taxi").html('');
        $.post(url+"Usuario.php", {perfil:1, usu: usu, pas: pas}, function(data){
            $("#contenedor_perfil_taxi").append(data);
            $("#contenedor_perfil_taxi").trigger('create');
        });
    });

    $(document).on('click', '.bt_ver_detalle', function(){
        var id = this.id;
        var usu = window.localStorage.getItem("usu");
        $("#contenedor_detalle").html('');
        $.post(url+'Taxi.php', {buscar: 1, id: id, usu: usu}, function(data){
            console.log(data);
            if(data == "no"){
                alert("Error: No existe la solicitud.");
            }else{
                $.mobile.changePage("#vt_ver_detalles", {role: "dialog", transition: "slidedown"});
                $("#contenedor_detalle").append(data);
                $("#contenedor_detalle").trigger('create');
            }
        });
    });

    $(document).on('click', '.bt_ver_detalle_taxi', function(){
        var id = this.id;
        var usu = window.localStorage.getItem("usu");
        $("#contenedor_detalle").html('');
        $.post(url+'Taxi.php', {buscartaxi: 1, id: id, usu: usu}, function(data){
            if(data == "no"){
                alert("Error: No existe la solicitud.");
            }else{
                $.mobile.changePage("#vt_ver_detalles", {role: "dialog", transition: "slidedown"});
                $("#contenedor_detalle").append(data);
                $("#contenedor_detalle").trigger('create');
            }
        });
    });

    $(document).on('click', '.bt_cancelar', function(){
        if(confirm('Esta seguro que desea cancelar esta solicitud?')){
            var id = this.id;
            var usu = window.localStorage.getItem("usu");

            $.post(url+"Taxi.php", {cancelar:1,id: id, usu: usu}, function(data){
                if(data == "ok"){
                    alert("Solicitud cancelada correctamente");
                    location.reload();
                }else{
                    alert("Error: no se pudo cancelar su solicitud");
                }
            });
        }
    });

    $(".bt_salir").on('click', function(){
        if(confirm("Esta seguro?")){
            window.localStorage.clear();
            $.mobile.changePage("#vt_login");
            navigator.app.exitApp();
        }
    });



    function obtenerMiDireccion(){
        var usu = window.localStorage.getItem('usu');
        var pas = window.localStorage.getItem('pas');
        $.post(url+'Usuario.php', {mi_direccion: 1, usu: usu, pas: pas}, function(data){
            window.localStorage.setItem("dir", data);
        });
    }

    function sesionActiva(){

    }
    $("#obtener_direccion").on('click', function(){
        var dir = window.localStorage.getItem("dir");
        $("#formulario_taxi textarea[name='dir']").val(dir);
    });

    $(document).on('click', "#bt_cambios", function(e){
        e.preventDefault();
        var usu = window.localStorage.getItem('usu');
        var pas = window.localStorage.getItem('pas');
        var da = $("#formulario_perfil").serializeArray();
        da.push({name: 'usu', value: usu}); 
        da.push({name: 'pas', value: pas}); 
        $.post(url+'Usuario.php', da, function(data){
            if(data.trim() == "ok"){
                alert("Cambios realizados correctamente");
                location.reload();
            }else{
                alert("Error: no se realizo ningun cambio");
            }
        });
    });

    $(document).on('click', '.bt_atender', function(){
        if(confirm('Esta seguro?, si acepta, debera realizar la carrera al cliente.')){
            var usu = window.localStorage.getItem('usu');
            var pas = window.localStorage.getItem('pas');
            var id = this.id;
            $.post(url+"Taxi.php", {atender: 1, id: id, usu: usu, pas: pas}, function(data){
                if(data == "ok"){
                    alert("El cliente espera por usted.");
                    location.reload();
                }else if(data == "pendiente"){
                    alert("Error: no puede atender otra solicitud, hasta que no se finalice la que esta en progreso");
                }else{
                    alert("No se pudo atender esta solicitud");
                }
            });

        }
    });

    $(document).on('click', '.bt_terminar', function(){
        if(confirm('Esta seguro que quiere marcar esta carrera como temrinada?')){
            var usu = window.localStorage.getItem('usu');
            var pas = window.localStorage.getItem('pas');
            var id = this.id;
            $.post(url+"Taxi.php", {terminar: 1, id: id, usu: usu, pas: pas}, function(data){
                if(data == "ok"){
                    alert("Carrera terminada exitosamente");
                    location.reload();
                }else{
                    alert("Error: no se pudo ejecutar esta accion");
                }
            });

        }
    });
    function realtime(){
        var np = $(".fila_en_progreso").length;
        window.setInterval(function(){
            var tipo = window.localStorage.getItem('tipo');
            var usu = window.localStorage.getItem('usu');
            var pas = window.localStorage.getItem('pas');

            if(tipo == "T"){
                var np = $(".fila_pendiente_taxi").length;
                $.post(url+"Taxi.php", {pendiente_taxi:1, usu: usu, np: np}, function(data){
                    if(data == "ok"){
                        alert("Hay nuevas carreras disponibles.");
                        location.reload();
                    }
                });
            }else{
                var np = $(".fila_en_progreso").length;
                $.post(url+"Taxi.php", {respuesta:1, usu: usu, np: np}, function(data){
                    if(data == "ok"){
                        alert("Un taxista atendio su solicitud, esperelo");
                        location.reload();
                    }
                });
            }
        }, 5000);
    }


});


